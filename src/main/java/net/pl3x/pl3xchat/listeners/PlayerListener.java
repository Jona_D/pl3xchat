package net.pl3x.pl3xchat.listeners;

import java.util.ArrayList;
import java.util.List;

import net.pl3x.pl3xchat.Pl3xChat;

import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerListener implements Listener {
	private Pl3xChat plugin;
	
	public PlayerListener(Pl3xChat plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void playerChat(AsyncPlayerChatEvent event) {
		if (event.isCancelled())
			return;
		// Setup vars
		Player sender = event.getPlayer();
		Boolean isGlobal = false;
		Boolean isUniverse = false;
		String message = sanitizeInput(event.getMessage());
		if (message.startsWith("\u00a7g")) {
			isGlobal = true;
			message = message.substring(2);
		} else if (plugin.enabledGlobal.contains(sender.getName())) {
			isGlobal = true;
		}
		if (message.startsWith("\u00a7u")) {
			isUniverse = true;
			message = message.substring(2);
		} else if (plugin.enabledUniverse.contains(sender.getName())) {
			isUniverse = true;
		}
		// Fix colorings
		if (sender.hasPermission("pl3xchat.color"))
			message = plugin.colorize(message);
		else
			message = plugin.removeColorCodes(message);
		if (plugin.decolorize(message).trim().equals("")) {
			event.setCancelled(true);
			event.getRecipients().clear();
			event.setMessage("");
			event.setFormat("");
			return;
		}
		// Setup prefix/suffix stuffs
		String format = plugin.getConfig().getString("local-format", "[{world}] <{name}> {message}");
		if (isGlobal)
			format = plugin.getConfig().getString("global-format", "[Global] <{name}> {message}");
		if (isUniverse)
			format = plugin.getConfig().getString("universe-format", "[Universe] <{name}> {message}");
		format = format.replaceAll("(?i)\\{name\\}", sender.getName());
		format = format.replaceAll("(?i)\\{dispname\\}", (sender.getDisplayName() == null) ? sender.getName() : sender.getDisplayName());
		format = format.replaceAll("(?i)\\{prefix\\}", getTag(sender, "prefix"));
		format = format.replaceAll("(?i)\\{suffix\\}", getTag(sender, "suffix"));
		format = format.replaceAll("(?i)\\{custom\\}", getTag(sender, "custom"));
		format = format.replaceAll("(?i)\\{world\\}", sender.getWorld().getName());
		format = format.replaceAll("(?i)\\{message\\}", message);
		event.setFormat(plugin.colorize(format));
		// Universe vs. Global vs. Local chat
		if (isUniverse)
			return;
		if (isGlobal) {
			World world = sender.getWorld();
			List<Player> toRemove = new ArrayList<Player>();
			for (Player target : event.getRecipients()) {
				if (target.equals(sender))
					continue;
				if (world.getPlayers().contains(target))
					continue;
				if (target.hasPermission("pl3xchat.seeall"))
					continue;
				toRemove.add(target);
			}
			event.getRecipients().removeAll(toRemove);
			return;
		}
		Double chatRadius = plugin.getConfig().getDouble("local-chat-radius", 40D);
		if (chatRadius == null)
			chatRadius = 0D;
		if (chatRadius <= 0D)
			return;
		List<Entity> ents = sender.getNearbyEntities(chatRadius, chatRadius, chatRadius);
		List<Player> toRemove = new ArrayList<Player>();
		for (Player target : event.getRecipients()) {
			if (target.equals(sender))
				continue;
			if (ents.contains(target))
				continue;
			if (target.hasPermission("pl3xchat.seeall"))
				continue;
			toRemove.add(target);
		}
		event.getRecipients().removeAll(toRemove);
	}
	
	private String getTag(Player player, String type) {
		String str = "";
		ConfigurationSection nodes = plugin.getConfig().getConfigurationSection(type);
		if (nodes == null)
			return "";
		if (player.isOp()) {
			str += plugin.getConfig().getString(type + ".op", "");
			return str;
		}
		for (String node : nodes.getKeys(false))
			if (player.hasPermission("pl3xchat." + type + "." + node))
				str += nodes.getString(node, "");
		return str;
	}
	
	public String sanitizeInput(String str) {
		str = str.replace("\\", "\\\\");
		str = str.replace("%", "%%");
		str = str.replace("$", "\\$");
		return str;
	}
}

package net.pl3x.pl3xchat.commands;

import net.pl3x.pl3xchat.Pl3xChat;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdUniverse implements CommandExecutor {
private Pl3xChat plugin;
	
	public CmdUniverse(Pl3xChat plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("universe")) {
			if (!cs.hasPermission("pl3xchat.universe")) {
				cs.sendMessage(ChatColor.RED + "You do not have permission for this command!");
				return true;
			}
			if (!(cs instanceof Player)) {
				cs.sendMessage(plugin.colorize("&4This command is only available to players."));
				return true;
			}
			Player p = (Player) cs;
			if (args.length < 1) {
				String name = p.getName();
				if (plugin.enabledUniverse.contains(name)) {
					plugin.enabledUniverse.remove(name);
					p.sendMessage(plugin.colorize("&dUniverse chat disabled."));
					return true;
				}
				plugin.enabledUniverse.add(name);
				p.sendMessage(plugin.colorize("&dUniverse chat enabled."));
				return true;
			}
			p.chat("\u00a7u" + getFinalArg(args, 0));
			return true;
		}
		return false;
	}
	
	private String getFinalArg(final String[] args, final int start) {
		final StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++) {
			if (i != start)
				bldr.append(" ");
			bldr.append(args[i]);
		}
		return bldr.toString();
	}
}

package net.pl3x.pl3xchat.commands;

import net.pl3x.pl3xchat.Pl3xChat;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdPl3xChat implements CommandExecutor {
	private Pl3xChat plugin;
	
	public CmdPl3xChat(Pl3xChat plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(final CommandSender cs, final Command cmd, final String label, final String[] args) {
		if (cmd.getName().equalsIgnoreCase("pl3xchat")) {
			if (!cs.hasPermission("pl3xchat.command.pl3xchat")) {
				cs.sendMessage(ChatColor.RED + "You do not have permission for this command!");
				return true;
			}
			if (args.length > 0) {
				if (args[0].equalsIgnoreCase("reload")) {
					plugin.reloadConfig();
					plugin.loadConfig();
					cs.sendMessage(ChatColor.LIGHT_PURPLE + "Configuration reloaded from disk.");
					return true;
				}
			}
			cs.sendMessage(ChatColor.LIGHT_PURPLE + "Pl3xChat v" + plugin.getDescription().getVersion());
			return true;
		}
		return false;
	}
}

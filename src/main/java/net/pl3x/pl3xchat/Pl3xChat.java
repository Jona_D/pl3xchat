package net.pl3x.pl3xchat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import net.pl3x.pl3xchat.commands.CmdGlobal;
import net.pl3x.pl3xchat.commands.CmdPl3xChat;
import net.pl3x.pl3xchat.commands.CmdUniverse;
import net.pl3x.pl3xchat.listeners.PlayerListener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

public class Pl3xChat extends JavaPlugin {
	private final PluginManager pm = Bukkit.getPluginManager();
	public final List<String> enabledGlobal = new ArrayList<String>();
	public final List<String> enabledUniverse = new ArrayList<String>();
	
	public void onEnable() {
		loadConfig();
		pm.registerEvents(new PlayerListener(this), this);
		getCommand("global").setExecutor(new CmdGlobal(this));
		getCommand("universe").setExecutor(new CmdUniverse(this));
		getCommand("pl3xchat").setExecutor(new CmdPl3xChat(this));
		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			log(ChatColor.RED + "Failed to start Metrics: " + ChatColor.YELLOW + e.getMessage());
		}
		log(ChatColor.YELLOW + "v" + this.getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		log(ChatColor.YELLOW + "Plugin Disabled.");
	}
	
	public void loadConfig() {
		if (!getConfig().contains("debug-mode"))
			getConfig().addDefault("debug-mode", false);
		if (!getConfig().contains("color-logs"))
			getConfig().addDefault("color-logs", true);
		if (!getConfig().contains("local-chat-radius"))
			getConfig().addDefault("local-chat-radius", 50D);
		if (!getConfig().contains("local-format"))
			getConfig().addDefault("local-format", "[{world}] <{name}> {message}");
		if (!getConfig().contains("global-format"))
			getConfig().addDefault("global-format", "[Global] <{name}> {message}");
		if (!getConfig().contains("universe-format"))
			getConfig().addDefault("universe-format", "[Universe] <{name}> {message}");
		if (!getConfig().contains("prefix.admin"))
			getConfig().addDefault("prefix.admin", "[Admin]");
		if (!getConfig().contains("suffix.donator"))
			getConfig().addDefault("suffix.donator", "[donator]");
		if (!getConfig().contains("custom.blue"))
			getConfig().addDefault("custom.blue", "&1");
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
	
	public void log (Object obj) {
		if (getConfig().getBoolean("color-logs", true))
			getServer().getConsoleSender().sendMessage(colorize("&3[&d" + getName() + "&3] &r" + obj));
		else
			Bukkit.getLogger().log(Level.INFO, "[" + getName() + "] " + (colorize((String) obj)).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
	}
	
	public String colorize(String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}
	
	public String decolorize(String str) {
		return str.replaceAll("(?i)\u00a7[a-f0-9k-or]", "");
	}
	
	public String removeColorCodes(String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "");
	}
}
